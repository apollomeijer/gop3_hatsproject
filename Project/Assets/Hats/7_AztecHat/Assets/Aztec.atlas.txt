
Aztec.png
format: RGBA8888
filter: Nearest,Nearest
repeat: none
aztec_base
  rotate: false
  xy: 0, 38
  size: 192, 204
  orig: 256, 256
  offset: 32, 19
  index: -1
aztec_eyes
  rotate: false
  xy: 360, 158
  size: 107, 84
  orig: 256, 256
  offset: 77, 66
  index: -1
aztec_feather1
  rotate: true
  xy: 468, 185
  size: 57, 30
  orig: 256, 256
  offset: 156, 184
  index: -1
aztec_feather2
  rotate: false
  xy: 0, 0
  size: 62, 37
  orig: 256, 256
  offset: 126, 198
  index: -1
aztec_feather3
  rotate: false
  xy: 193, 66
  size: 53, 41
  orig: 256, 256
  offset: 72, 197
  index: -1
aztec_feather4
  rotate: false
  xy: 360, 120
  size: 56, 37
  orig: 256, 256
  offset: 40, 179
  index: -1
aztec_stones
  rotate: false
  xy: 193, 108
  size: 166, 134
  orig: 256, 256
  offset: 45, 48
  index: -1
