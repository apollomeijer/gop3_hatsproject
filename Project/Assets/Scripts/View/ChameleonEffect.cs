﻿using UnityEngine;
using System.Collections;

public class ChameleonEffect : MonoBehaviour {

	[SerializeField]private bool isShifting;
	public int speedValue;
	[SerializeField]private Renderer chameleonRenderer;
	
	[SpineSlot]
	[SerializeField]private string eyeSlot;

	[SpineAttachment(currentSkinOnly: true, slotField: "eyeSlot")]
	[SerializeField]private string[] eyeAttachments;

	private SkeletonAnimation skeletonAnimation;

	void Start () {
		skeletonAnimation = GetComponent<SkeletonAnimation>();
		StartCoroutine("Blink");
	}

	void FixedUpdate () {
		if(isShifting){
		chameleonRenderer.material.SetFloat("_HueShift",Mathf.PingPong(Time.time * speedValue, 5000));
		}
	}

	IEnumerator Blink () {
		while (true) {
			yield return new WaitForSeconds(Random.Range(0.25f, 1.5f));
			string attachmentType = eyeAttachments[Random.Range(0,eyeAttachments.Length)];
			skeletonAnimation.skeleton.SetAttachment(eyeSlot,attachmentType);
			if(attachmentType == "chameleon_wink"){
				yield return new WaitForSeconds(Random.Range(0.10f, 0.15f));
				skeletonAnimation.skeleton.SetAttachment(eyeSlot,eyeAttachments[0]);
				yield return new WaitForSeconds(Random.Range(1.15f, 1.30f));
			}
		}
	}
}
