﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class HatComponent : MonoBehaviour {
	
	[SerializeField]private List<HatAnimationModel> animationModels;
	private HatAnimationModel currentModel;
	private SkeletonAnimation spineAnimation;
	private string currentlyPlaying;
	
	void Start () {
		spineAnimation = GetComponent<SkeletonAnimation>();
		spineAnimation.state.Start += OnAnimationStart;
		spineAnimation.state.Complete += OnAnimationComplete;
	}

	void FixedUpdate () {
		///DEBUG

		if(Input.GetKeyDown(KeyCode.A)){
			PlayAnimation(0);
		}
		if(Input.GetKeyDown(KeyCode.D)){
			PlayAnimation(1);
		}

	}
	
	public void PlayAnimation (int state) {
		currentModel = GetAnimationModel(state);
		spineAnimation.AnimationName = currentModel.animationState;
	}

	public void PlayAnimation (string state) {
		currentModel = GetAnimationModel(state);
		spineAnimation.AnimationName = currentModel.animationState;
	}

	void OnAnimationStart (Spine.AnimationState state, int trackIndex) {
		currentlyPlaying = currentModel.animationState;
		if(currentModel.particleEffect.Length != 0){
			EmitHatParticles();
		}
		if(currentModel.soundEffect){
			PlayAudioClip();
		}
	}

	void OnAnimationComplete (Spine.AnimationState state, int trackIndex, int loopCount) {
		//Return to Idle Animation
		if(currentlyPlaying != animationModels[0].animationState){
			PlayAnimation(animationModels[0].animationState);
		}
	}

	private void PlayAudioClip () {
		//Play Audio Code
	}

	private void EmitHatParticles () {
		for(int i = 0; i < currentModel.particleEffect.Length; i++){
			currentModel.particleEffect[i].Emit(currentModel.particleEffect[i].maxParticles);
		}
	}

	private HatAnimationModel GetAnimationModel (int index) {
		if(index > -1 && index < animationModels.Count){
			return animationModels[index];
		}else{
			Debug.LogWarning("WARNING: Animation Model not found, returning first Animation Model");
			return animationModels[0];
		}
	}

	private HatAnimationModel GetAnimationModel (string name) {
		for(int i = 0; i < animationModels.Count; i++) {
			if(animationModels[i].animationState == name) {
				return animationModels[i];
			}
		}
		Debug.LogWarning("WARNING: Animation Model not found, returning first Animation Model");
		return animationModels[0];
	}
	
}
