﻿using UnityEngine;
using System.Collections;

public class HatModel {
	public int ID = -1;
	public HatAnimationModel animationModel;

	public HatModel ( int _id , HatAnimationModel _animationModel ) { 
		ID = _id;
		animationModel = _animationModel;
	}

}
