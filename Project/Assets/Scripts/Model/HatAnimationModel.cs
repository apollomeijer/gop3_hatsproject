﻿using UnityEngine;
using System.Collections;
using Spine;

[System.Serializable]
public class HatAnimationModel {
	
	[SpineAnimation]
	public string animationState;
	public AudioClip soundEffect;
	public ParticleSystem[] particleEffect;
	
}
